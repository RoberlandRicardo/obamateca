package br.ufrn.imd.obamateca.Obamateca.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.ufrn.imd.obamateca.Obamateca.exceptions.EmailNotFoundException;

@ControllerAdvice
public class ControllerExceptionHandler {
	
	//Exception throwed when the seach of a email is not found
	@ExceptionHandler(EmailNotFoundException.class)
	public void EmailNotFound (EmailNotFoundException e, HttpServletRequest request) {
		HttpStatus status = HttpStatus.NOT_FOUND;
	}
	
}
