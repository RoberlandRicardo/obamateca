package br.ufrn.imd.obamateca.Obamateca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import br.ufrn.imd.obamateca.Obamateca.model.Services;
import br.ufrn.imd.obamateca.Obamateca.model.entities.Book;

@RestController
@RequestMapping(value = "/book")
public class BookController {
	
	@Autowired
	Services service;
	
	private static final String viewInsert = "";
	private static final String viewList = "";
	
	//------------------------------------- Exhibit
	@GetMapping
	public ResponseEntity<Void> redirectToList(UriComponentsBuilder ucb) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.LOCATION, ucb.toUriString() + "/list");
		return new ResponseEntity<>(headers, HttpStatus.PERMANENT_REDIRECT);
	}
	
	@GetMapping("/list")
    public ModelAndView loadViewList() {
        final ModelAndView mav = new ModelAndView();
        mav.addObject(service.listAllBook());
        mav.setViewName(viewList);
        mav.setStatus(HttpStatus.OK);
        return mav;
    }
	
	//------------------------------------- Insert
    @GetMapping("/insert")
    public ModelAndView loadFormInsert() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(viewInsert);
        return modelAndView;
    }
    
    @PostMapping(value = "/insert")
	public ResponseEntity<Book> insert(Book book, UriComponentsBuilder ucb) {
		Book newBook = service.createNewBook(book);
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.LOCATION, ucb.toUriString());
		return new ResponseEntity<>(newBook, headers, HttpStatus.CREATED);
	}
	
	/*@GetMapping(value = "/insert")
	public ResponseEntity<Void> create(UriComponentsBuilder ucb) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.LOCATION, "http://localhost:8080/user/ricardo@gmail.com");
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}*/
	
	/*@GetMapping(value = "/insert")
	public ResponseEntity<Void> create(UriComponentsBuilder ucb) {
		
		return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header(HttpHeaders.LOCATION, "http://localhost:8080/user/ricardo@gmail.com").build();
	}*/
	
	/*@PostMapping(value = "/insert")
	public ResponseEntity<Book> create(Book book) {
		Book newBook = service.createNewBook(book);
		HttpHeaders headers = new HttpHeaders();
		headers.add("teste", "http://localhost:8080/user/person/ricardo@gmail.com");
		return new ResponseEntity<>(newBook, headers, HttpStatus.CREATED);
	}*/
}
