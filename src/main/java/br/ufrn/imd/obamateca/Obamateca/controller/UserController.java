package br.ufrn.imd.obamateca.Obamateca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import br.ufrn.imd.obamateca.Obamateca.model.Services;
import br.ufrn.imd.obamateca.Obamateca.model.entities.User;

@Controller
public class UserController {
	
	@Autowired
	Services service;
	
	//Caso o static cause problemas, ele deve ser removido
	private static List<GrantedAuthority> authorityListAdmin = AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_ADMIN");
	private static List<GrantedAuthority> authorityListUser = AuthorityUtils.createAuthorityList("ROLE_USER");

    @GetMapping("/user")
    public ModelAndView user() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user");
        /*Para adicionar um novo usuario, é só usar o metodo service.CreateNewUser(Objeto user), 
         */
        return modelAndView;
    }
    
    //@PreAuthorize("hasAuthority('USER')")
    @GetMapping(value = "user/{email}")
	public ResponseEntity<User> findByEmail(@PathVariable String email){
		User obj = service.findUserByEmail(email);
		User teste = new User("asdia", "asdasd", "asldas", "adsasd", "asdasd", false);
		
		return ResponseEntity.ok().body(teste);
	}
    
}
