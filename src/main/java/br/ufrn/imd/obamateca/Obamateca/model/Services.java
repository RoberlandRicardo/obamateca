package br.ufrn.imd.obamateca.Obamateca.model;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import br.ufrn.imd.obamateca.Obamateca.exceptions.EmailNotFoundException;
import br.ufrn.imd.obamateca.Obamateca.model.entities.Book;
import br.ufrn.imd.obamateca.Obamateca.model.entities.User;
import br.ufrn.imd.obamateca.Obamateca.model.repositories.BookRepository;
import br.ufrn.imd.obamateca.Obamateca.model.repositories.BorrowingRepository;
import br.ufrn.imd.obamateca.Obamateca.model.repositories.UserRepository;

@Service
public class Services implements UserDetailsService{
	@Autowired private BookRepository bookRepository;
	@Autowired private BorrowingRepository borrowingRepository;
	@Autowired private UserRepository userRepository;
	
	//------------------------------------- User Methods
	//Create a new user
	public User createNewUser(User user) {
		return userRepository.save(user);
	}
	
	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws EmailNotFoundException {
		User user = Optional.ofNullable(userRepository.findByEmail(username)).orElseThrow(() -> new EmailNotFoundException("Email not found"));
		return user;
	}
	
	//------------------------------------- Book Methods
	public Book createNewBook(Book book) {
		return bookRepository.save(book);
	}
	
	public List<Book> listAllBook(){
		return bookRepository.findAll();
	}
}
