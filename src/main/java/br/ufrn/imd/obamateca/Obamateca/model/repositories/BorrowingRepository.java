package br.ufrn.imd.obamateca.Obamateca.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.imd.obamateca.Obamateca.model.entities.Borrowing;

@Repository
public interface BorrowingRepository extends JpaRepository<Borrowing, Long>{

}
