package br.ufrn.imd.obamateca.Obamateca.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufrn.imd.obamateca.Obamateca.model.entities.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{

}
