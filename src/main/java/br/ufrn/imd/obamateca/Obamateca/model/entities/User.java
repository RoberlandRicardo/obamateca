package br.ufrn.imd.obamateca.Obamateca.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "tb_user")
public class User implements Serializable, UserDetails{

	private static final long serialVersionUID = 1L;

	//Atributos da tabela
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	private String name;
	@NotEmpty
	private String surname;
	@NotEmpty
	private String email;
	@NotEmpty
	private String password;
	private Boolean adm;
	private Boolean haveKey;
	@NotEmpty
	private String availableTime;
	@OneToMany(mappedBy = "user") 
	private List<Borrowing> borrowings = new ArrayList<>();
	@OneToMany(mappedBy = "user")
	private List<Book> books = new ArrayList<>();
	@Transient
	private Collection<? extends GrantedAuthority> authorities;
	
	//Construtores
	public User() {
		
	}
	
	public User(String name, String surname, String availableTime, String email, String password, Boolean adm) {
		this.name = name;
		this.surname = surname;
		this.availableTime = availableTime;
		this.email = email;
		this.password = password;
		this.adm = adm;
	}

	public User(String name, String surname, String availableTime, String email, String password, Boolean adm, Collection<? extends GrantedAuthority> authorities) {
		this.name = name;
		this.surname = surname;
		this.availableTime = availableTime;
		this.email = email;
		this.password = password;
		this.adm = adm;
		this.authorities = authorities;
	}

	public User(Long id, String name, String surname, String email, String password, Boolean adm, Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password = password;
		this.adm = adm;
		this.authorities = authorities;
	}

	//Gets e Setts
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Boolean getHaveKey() {
		return haveKey;
	}

	public void setHaveKey(Boolean haveKey) {
		this.haveKey = haveKey;
	}

	public String getAvailableTime() {
		return availableTime;
	}

	public void setAvailableTime(String availableTime) {
		this.availableTime = availableTime;
	}

	public Boolean getAdm() {
		return adm;
	}
	
	public void setAdm(Boolean adm) {
		this.adm = adm;
	}

	public List<Borrowing> getBorrowings(){
		return borrowings;
	}
	
	public void addBorrowing(Borrowing borrowing) {
		borrowings.add(borrowing);
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}
	
	
	// This methods will be used in the future
	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	
	//HashCode e metodo equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
