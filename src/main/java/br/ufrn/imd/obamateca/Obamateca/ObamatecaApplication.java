package br.ufrn.imd.obamateca.Obamateca;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import br.ufrn.imd.obamateca.Obamateca.model.Services;
import br.ufrn.imd.obamateca.Obamateca.model.entities.User;

@SpringBootApplication
public class ObamatecaApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ObamatecaApplication.class, args);
		
	}
	
	
	// Temporarily created for test
	@Bean
	public CommandLineRunner demo(Services service) {
		/*Create a user to login:
		 * Username: ricardo@gmail.com
		 * Password: uiui
		 */
		return (args) -> {
			List<GrantedAuthority> authorityListAdmin = AuthorityUtils.createAuthorityList("USER", "ROLE_ADMIN");
			User user = new User("ricardo", "junior", "34T56", "ricardo@gmail.com",
			"$2a$10$zJrMWjw2nnQGDdVC8tRJ8OjQXnR.8ge4UCMfIDiPHggQYJkHY/06." , true,
			authorityListAdmin);
			service.createNewUser(user);
		};
	}
}
