package br.ufrn.imd.obamateca.Obamateca.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "tb_book")
public class Book implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//Atributo da tabela
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	private String title;
	private String author_name;
	private String author_surname;
	private String coletion;
	private String sinopse;
	private String url_image;
	@ManyToOne
	@JoinColumn(name = "user_id")
	@NotEmpty
	private User user;
	@OneToMany(mappedBy = "book")
	private List<Borrowing> borrowings = new ArrayList<>();
	
	//Construtores
	public Book () {
		
	}

	public Book(String title, String author_name, String author_surname, String sinopse, String url_image, String coletion) {
		this.title = title;
		this.author_name = author_name;
		this.author_surname = author_surname;
		this.sinopse = sinopse;
		this.url_image = url_image;
		this.coletion = coletion;
	}
	
	public Book(Long id, String title, String author_name, String author_surname, String sinopse, String url_image, String coletion) {
		this.id = id;
		this.title = title;
		this.author_name = author_name;
		this.author_surname = author_surname;
		this.sinopse = sinopse;
		this.url_image = url_image;
		this.coletion = coletion;
	}

	//Gets e Setts
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor_name() {
		return author_name;
	}

	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}

	public String getAuthor_surname() {
		return author_surname;
	}

	public void setAuthor_surname(String author_surname) {
		this.author_surname = author_surname;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public String getColetion() {
		return coletion;
	}

	public void setColetion(String coletion) {
		this.coletion = coletion;
	}

	public String getUrl_image() {
		return url_image;
	}

	public void setUrl_image(String url_image) {
		this.url_image = url_image;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Borrowing> getBorrowings(){
		return borrowings;
	}
	
	public void addBorrowing(Borrowing borrowing) {
		borrowings.add(borrowing);
	}

	//Hashcode e metodo equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
