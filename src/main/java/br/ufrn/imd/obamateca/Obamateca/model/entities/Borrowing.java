package br.ufrn.imd.obamateca.Obamateca.model.entities;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tb_borrowing")
public class Borrowing implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	//Atributo da tabela
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss ", timezone = "GMT")
	@NotEmpty
	private Instant data_emprestimo;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss ", timezone = "GMT")
	private Instant data_vencimento;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss ", timezone = "GMT")
	private Instant data_entrega;
	@ManyToOne
	@JoinColumn(name = "user_id")
	@NotEmpty
	private User user;
	@ManyToOne
	@JoinColumn(name = "book_id")
	@NotEmpty
	private Book book;
	
	
	//Contrutores
	public Borrowing() {
		
	}
	
	public Borrowing(Instant data_emprestimo, Instant data_vencimento, Instant data_entrega, User user, Book book) {
		this.data_emprestimo = data_emprestimo;
		this.data_vencimento = data_vencimento;
		this.data_entrega = data_entrega;
		this.user = user;
		this.book = book;
	}

	public Borrowing(Long id, Instant data_emprestimo, Instant data_vencimento, Instant data_entrega, User user, Book book) {
		this.id = id;
		this.data_emprestimo = data_emprestimo;
		this.data_vencimento = data_vencimento;
		this.data_entrega = data_entrega;
		this.user = user;
		this.book = book;
	}

	//Gets e Setts
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getData_emprestimo() {
		return data_emprestimo;
	}

	public void setData_emprestimo(Instant data_emprestimo) {
		this.data_emprestimo = data_emprestimo;
	}

	public Instant getData_entrega() {
		return data_entrega;
	}

	public Instant getData_vencimento() {
		return data_vencimento;
	}

	public void setData_vencimento(Instant data_vencimento) {
		this.data_vencimento = data_vencimento;
	}

	public void setData_entrega(Instant data_entrega) {
		this.data_entrega = data_entrega;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	//HashCode e metodo equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Borrowing other = (Borrowing) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
